# Appointment management
เป็นบริการการทำการนัดหมาย โดยมาลำดับความสำคัญเข้ามาเกี่ยวข้อง

## condition
- datetime_in ไม่สามารถจองหลังจาก datetime_out ได้
- datetime ไม่สามารถจองเวลาในอดีตได้ ดึง API จาก https://timezonedb.com/api ( ติดปัญหาเอาค่าออกมาใช้ )
- priority มีตั้งแต่ 1-5

---

## Appointment Entity
```json
{
	"datetime_in" : "string",
	"datetime_out" : "string",
	"report" : "string",
	"priority" : "number",
	"createby" : "string"
}
```

---

## Appointment Service

### Create
- POST /appointments
```json
{
	"datetime_in": "2021-12-13 12:00 PM",
	"datetime_out": "2021-12-13 14:00 PM",
	"report": "Arrange a meeting",
	"priority": 5,
	"createby": "Nack Thiti"
}
```

### Read all
- GET /appointments

### Read by ID
- GET /appointments/{id}

### Update
- PATCH /appointments/{id}
```json
{
	"datetime_in": "2021-12-13 12:00 PM",
	"datetime_out": "2021-12-13 14:00 PM"
}
```

### Delete
- DELETE /appointments/{id}
