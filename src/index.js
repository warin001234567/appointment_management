const express = require('express')
require('./db/mongoose')
const appointmentRouter = require('./routers/appointment')

const app = express()
const port = process.env.port || 3000

app.use(express.json())
app.use(appointmentRouter)

app.listen(port, () => {
    console.log('Server is up on port ' + port)
})